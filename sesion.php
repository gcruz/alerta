<?php
session_start();
?>
<?php
	if(!isset($_SESSION['s_id']) || !isset($_SESSION['s_tipo']) || !isset($_SESSION['s_apenom']) || !isset($_SESSION['s_aula'])){
		echo "<script>location.href='index.php';</script>";
	}

	$c_id     = $_SESSION['s_id'];
	$c_tipo   = $_SESSION['s_tipo'];
	$c_apenom = mb_convert_case(trim($_SESSION['s_apenom']), MB_CASE_TITLE, "UTF-8");
	$c_aula   = $_SESSION['s_aula'];
	$aul      = $_SESSION['s_aula'];
	
	if($c_tipo=="1"){
		$ico_aula    = "1";
		$ico_usuario = "1";
		$ico_buscar  = "0";
		$ico_alerta  = "0";
		$ult_alerta  = "0";
		$lis_alerta  = "0";
		$mis_alerta  = "0";
	}elseif($c_tipo=="2"){
		$ico_aula    = "0";
		$ico_usuario = "1";
		$ico_buscar  = "1";
		$ico_alerta  = "1";
		$ult_alerta  = "0";
		$lis_alerta  = "0";
		$mis_alerta  = "1";
	}elseif($c_tipo=="3"){
		$ico_aula    = "0";
		$ico_usuario = "0";
		$ico_buscar  = "1";
		$ico_alerta  = "0";
		$ult_alerta  = "1";
		$lis_alerta  = "1";
		$mis_alerta  = "0";
	}else{
		$ico_aula    = "1";
		$ico_usuario = "1";
		$ico_buscar  = "1";
		$ico_alerta  = "1";
		$ult_alerta  = "1";
		$lis_alerta  = "1";
		$mis_alerta  = "1";
	}
?>