<?php
	include("bd/bd.php");
	include("inc/funcion.php");
	$obj_bd = new BD();
	
	$usu = "";
	$cla = "";
	if(isset($_COOKIE['usu_id']) && isset($_COOKIE['marca'])){
		if($_COOKIE['usu_id']!="" || $_COOKIE['marca']!=""){
			$res = $obj_bd->consulta("select * from usuario where usu_id='".$_COOKIE["usu_id"]."' and cookie='".$_COOKIE["marca"]."' and cookie<>''");
			if($obj_bd->num_rows($res)>0){
				$usu = $obj_bd->result($res, "usu_dni");
				$cla = desencriptar($obj_bd->result($res, "usu_cla"), "alerta");
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ALERTA - Mantente Comunicado</title>
	
    <link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">ALERTA</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#portfolio">Beneficios</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">Acerca</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact">Contacto</a>
                    </li>
                    <li class="page-scroll">
                    	<a href="#login" data-toggle="modal">Login</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="img/logo1.png" alt="">
                    <div class="intro-text">
                        <span class="name">ALERTA</span>
                        <hr class="star-light">
                        <span class="skills">Mantente comunicado con las actividades de tus hijos</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Beneficios</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div style="margin-top:60px" class="row">
                <div class="col-sm-4 portfolio-item">
                    <img src="img/portfolio/familia.jpg" class="img-responsive" alt="">
                    <div class="caption">
        				<h4 style="text-align:center; line-height:34px">Permite a los padres de familias estar enterados en tiempo real de cualquier urgencia que puedan sufrir sus hijos en la escuela.</h4>
        			</div>
                </div>
                <div class="col-sm-4 portfolio-item">
                    <img src="img/portfolio/warning.jpg" class="img-responsive" alt="">
                    <div class="caption">
        				<h4 style="text-align:center; line-height:34px">Vehículo de comunicación, mensajería e información, entre los padres y los tutores.</h4>
        			</div>
                </div>
                <div class="col-sm-4 portfolio-item">
                    <img src="img/portfolio/win.jpg" class="img-responsive" alt="">
                    <div class="caption">
        				<h4 style="text-align:center; line-height:34px">Instrumento de referencias formativas.</h4>
        			</div>
                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Acerca</h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-2">
                    <p>Dónde está tu hijo? Cómo está? Alerta está dirigida a garantizar la comunicación permanente entre los padres de familia y el tutor de sus hijos en las escuelas. Se trata de un instrumento de reporte instantáneo de urgencias de los estudiantes.</p>
                </div>
                <div class="col-lg-4">
                    <p>Constituye además, un eficaz medio de comunicación, mensajería e información, de los diferentes aspectos de la vida escolar. Y como puede ser utilizado para  enviar recursos digitales, se convierte en un apreciable vehículo  de desarrollo de capacidades.</p>
                </div>
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <a href="https://play.google.com/store/apps/details?id=com.redes.alerta2" target="_blank" class="btn btn-lg btn-outline">
                        <i class="fa fa-download"></i> Google Play
                    </a>
                </div>
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <a href="https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=969816184&mt=8" target="_blank" class="btn btn-lg btn-outline">
                        <i class="fa fa-download"></i> App Store
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Contacto</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Nombre</label>
                                <input type="text" class="form-control" placeholder="Nombres" id="name" required data-validation-required-message="Por favor ingrese su nombre.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Email" id="email" required data-validation-required-message="Por favor ingrese su email.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Teléfono</label>
                                <input type="tel" class="form-control" placeholder="Teléfono" id="phone" required data-validation-required-message="Por favor ingrese su teléfono.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Mensaje</label>
                                <textarea rows="5" class="form-control" placeholder="Mensaje" id="message" required data-validation-required-message="Por favor ingrese su mensaje."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <button type="submit" class="btn btn-success btn-lg">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Dirección</h3>
                        <p>Av. Brasil 1640 dpto. 300<br>Pueblo Libre - Lima</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Redes en la web</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Desarrollado por Redes.la</h3>
                        <p>Alerta es una App de comunicación, desarrollada y creada por <a href="http://redes.la">REDES.LA</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Alerta 2014
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <!-- Portfolio Modals -->
<!-- LOGIN -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Ingresar</h4>
			</div>
            <div class="modal-body">
                <form id="loginform" class="form-horizontal" role="form">
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input name="usu" id="usu" type="text" class="form-control" value="<?php echo $usu; ?>" placeholder="Username o email" onKeyPress="enter(event)" />
                    </div>
                    <div style="margin-bottom: 12px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input name="cla" id="cla" type="password" class="form-control" value="<?php echo $cla; ?>" placeholder="Password" onKeyPress="enter(event)" />
                    </div>
                    <div class="input-group">
                        <div class="checkbox">
                        <label><input type="checkbox" name="remember" id="remember" value="1">Recordar datos</label>
                    </div>
                </form>
            </div>
		</div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" onClick="ingresar()">Ingresar</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
        </div>
	</div>
</div>
</div>
<!-- END LOGIN -->
    
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>
    
    <script>
	function ingresar(){
		var usu = $("input#usu").val();
		var cla = $("input#cla").val();
		
		$("input#usu").css('border-color', '');
		$("input#cla").css('border-color', '');
		if(usu.length==0){
			$("input#usu").css('border-color', '#F00');
			$("input#usu").focus();
			return;
		}
		if(cla.length==0){
			$("input#cla").css('border-color', '#F00');
			$("input#cla").focus();
			return;
		}
		var chk = $("#remember").prop("checked") ? 1 : 0;

		location.href = 'logeo.php?usu='+usu+'&cla='+cla+'&chk='+chk;
	}
	function enter(e){
		if(((document.all)?e.keyCode:e.which)=="13"){
			ingresar();
		}
	}
	</script>

</body>

</html>
