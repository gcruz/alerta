<?php
	if(isset($_REQUEST["modo"])){
		$modo = $_REQUEST["modo"];
	}else{
		$modo = "add";
	}
	
	$id     = "";
	$nom    = "";
	$insedu = "";
	$codmod = "";
	$gra    = "";
	$sec    = "";
	$canusu = "";
	if($modo=="upd"){
		$id      = $_REQUEST["id"];
		$num_aul = $obj_aula->editar("aul_id='$id'");
		$nom     = $obj_aula->e_nom();
		$insedu  = $obj_aula->e_insedu();
		$codmod  = $obj_aula->e_codmod();
		$gra     = $obj_aula->e_gra();
		$sec     = $obj_aula->e_sec();
		$canusu  = $obj_aula->e_canusu();
	}
?>
<!--FORM-->
<div class="panel panel-default" data-widget-editbutton="false" id="p1">
	<div class="panel-heading">
    	<h2>Nueva Aula</h2>
	</div>
    <div class="panel-body">
        <form action="control/aula.php" class="form-horizontal row-border" name="frm" id="frm">
        <input type="hidden" name="modo" id="modo" value="<?php echo $modo; ?>" class="form-control">
        <input type="hidden" name="id" id="id" value="<?php echo $id; ?>" class="form-control">
        	<div class="form-group">
            	<label class="col-sm-2 control-label">Nombre Aula</label>
                <div class="col-sm-8">
                	<input type="text" name="nom" id="nom" class="form-control" maxlength="100" value="<?php echo $nom; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Instituci&oacute;n Educativa</label>
                <div class="col-sm-8">
                	<input type="text" name="insedu" id="insedu" class="form-control" maxlength="200" value="<?php echo $insedu; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">C&oacute;digo Modular</label>
                <div class="col-sm-8">
                	<input type="text" name="codmod" id="codmod" class="form-control" maxlength="50" value="<?php echo $codmod; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Grado</label>
                <div class="col-sm-8">
                	<select class="form-control" name="gra" id="gra">
                    <option value="">[ Seleccione ]</option>
                    <option value="1" <?php if($gra=="1"){ echo "selected"; } ?>>Primero</option>
                    <option value="2" <?php if($gra=="2"){ echo "selected"; } ?>>Segundo</option>
                    <option value="3" <?php if($gra=="3"){ echo "selected"; } ?>>Tercero</option>
                    <option value="4" <?php if($gra=="4"){ echo "selected"; } ?>>Cuarto</option>
                    <option value="5" <?php if($gra=="5"){ echo "selected"; } ?>>Quinto</option>
                    <option value="6" <?php if($gra=="6"){ echo "selected"; } ?>>Sexto</option>
					</select>
				</div>
			</div>
            
			<div class="form-group">
            	<label class="col-sm-2 control-label">Secci&oacute;n</label>
                <div class="col-sm-8">
                	<input type="text" name="sec" id="sec" class="form-control" maxlength="50" value="<?php echo $sec; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Cantidad Usuarios</label>
                <div class="col-sm-8">
                	<input type="text" name="canusu" id="canusu" class="form-control" maxlength="5" value="<?php echo $canusu; ?>">
				</div>
			</div>
		</form>
        
        <div class="panel-footer">
        	<div class="row">
            	<div class="col-sm-8 col-sm-offset-2">
                	<button class="btn-primary btn" onclick="grabar()">Guardar</button>
                    <button class="btn-default btn" onclick="cancelar()">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function grabar(){
	var nom    = $("input#nom").val();
	var insedu = $("input#insedu").val();
	var codmod = $("input#codmod").val();
	var gra    = $("select#gra").val();
	var sec    = $("input#sec").val();
	var canusu = $("input#canusu").val();
	
	$("input#nom").css('border-color', '');
	$("input#insedu").css('border-color', '');
	$("input#codmod").css('border-color', '');
	$("select#gra").css('border-color', '');
	$("input#sec").css('border-color', '');
	$("input#canusu").css('border-color', '');
	if(nom.length==0){
		$("input#nom").css('border-color', '#F00');
		$("input#nom").focus();
		return;
	}
	if(insedu.length==0){
		$("input#insedu").css('border-color', '#F00');
		$("input#insedu").focus();
		return;
	}
	if(codmod.length==0){
		$("input#codmod").css('border-color', '#F00');
		$("input#codmod").focus();
		return;
	}
	if(gra.length==0){
		$("select#gra").css('border-color', '#F00');
		$("select#gra").focus();
		return;
	}
	if(sec.length==0){
		$("input#sec").css('border-color', '#F00');
		$("input#sec").focus();
		return;
	}
	if(canusu.length==0){
		$("input#canusu").css('border-color', '#F00');
		$("input#canusu").focus();
		return;
	}

	document.frm.submit();
}

function cancelar(){
	alert("A donde voy al cancelar.");
}
</script>
<!-- ENDFORM -->