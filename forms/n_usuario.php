<?php
	if(isset($_REQUEST["modo"])){
		$modo = $_REQUEST["modo"];
	}else{
		$modo = "add";
	}

	$id  = "";
	$tip = "";
	$aul = "";
	$nom = "";
	$ape = "";
	$dni = "";
	$ema = "";
	$cel = "";
	$sex = "";
	$dir = "";
	$cla = "";
	if($modo=="upd"){
		$id      = $_REQUEST["id"];
		$num_usu = $obj_usuario->editar("usu_id=$id");
		$tip = $obj_usuario->e_tip();
		$aul = $obj_usuario->e_aul();
		$nom = $obj_usuario->e_nom();
		$ape = $obj_usuario->e_ape();
		$dni = $obj_usuario->e_dni();
		$ema = $obj_usuario->e_ema();
		$cel = $obj_usuario->e_cel();
		$sex = $obj_usuario->e_sex();
		$dir = $obj_usuario->e_dir();
		$cla = desencriptar($obj_usuario->e_cla(), "alerta");
	}
	
	$w = "";
	if($c_tipo=="1"){
		$w = "1=1 and ";
	}elseif($c_tipo=="2"){
		$w = "aul_id=$c_aula and ";
	}
	$res_aul = $obj_aula->consultar("aul_id, aul_nom, aul_canusu", "$w aul_est='1'", "aul_nom");
	$num_aul = $obj_bd->num_rows($res_aul);
	
	$w = "";
	if($c_tipo=="1"){
		$w = "usu_id<>$c_id and ";
	}elseif($c_tipo=="2"){
		$w = "usu_id<>$c_id and usu_aul=$c_aula and ";
	}
	$res_usu = $obj_usuario->consultar("usu_id, usu_nom, usu_ape, usu_dni, usu_ema, usu_est", "$w 1=1", "usu_nom, usu_ape");
	$num_usu = $obj_bd->num_rows($res_usu);
?>
<!--FORM-->
<div class="panel panel-default" data-widget-editbutton="false">
	<div class="panel-heading">
		<h2>Nuevo Usuario</h2>
	</div>
    <div class="panel-body">
    	<form action="control/usuario.php" class="form-horizontal row-border" name="frm" id="frm">
        <input type="hidden" name="modo" id="modo" value="<?php echo $modo; ?>" class="form-control">
        <input type="hidden" name="id" id="id" value="<?php echo $id; ?>" class="form-control">
			<div class="form-group">
            	<label class="col-sm-2 control-label">Perfil</label>
                <div class="col-sm-8">
                	<select class="form-control" name="tip" id="tip">
                    <option value="">[ Seleccione ]</option>
					<?php if($c_tipo=="1"){ ?>
                    <option value="1" <?php if($tip=="1"){ echo "selected"; } ?>>Administrador</option>
                    <option value="2" <?php if($tip=="2"){ echo "selected"; } ?>>Tutor</option>
					<?php } ?>
					<?php if($c_tipo=="1" || $c_tipo=="2"){ ?><option value="3" <?php if($tip=="3"){ echo "selected"; } ?>>Padre de Familia</option><?php } ?>
                    </select>
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Aula</label>
                <div class="col-sm-8">
                	<select class="form-control" name="aul" id="aul">
                    <option value="">[ Seleccione ]</option>
                    <?php
						$canusu = 0;
						if($num_aul>0){
							while($f=$obj_bd->fetch_assoc($res_aul)){
								$canusu = $f["aul_canusu"];
								if($f["aul_id"]==$aul){ $s="selected"; }else{ $s=""; }
                    ?>
			                    <option value="<?php echo $f["aul_id"]; ?>" <?php echo $s; ?>><?php echo $f["aul_nom"]; ?></option>
                    <?php
							}
						}
                    ?>
                    </select>
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Nombres</label>
                <div class="col-sm-8">
                	<input type="text" name="nom" id="nom" class="form-control" maxlength="50" value="<?php echo $nom; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Apellidos</label>
                <div class="col-sm-8">
                	<input type="text" name="ape" id="ape" class="form-control" maxlength="100" value="<?php echo $ape; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">DNI</label>
                <div class="col-sm-8">
                	<input type="text" name="dni" id="dni" class="form-control" maxlength="8" value="<?php echo $dni; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-8">
                	<input type="text" name="ema" id="ema" class="form-control" maxlength="50" value="<?php echo $ema; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Celular</label>
                <div class="col-sm-8">
                	<input type="text" name="cel" id="cel" class="form-control" maxlength="50" value="<?php echo $cel; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">G&eacute;nero</label>
                <div class="col-sm-8">
                	<select class="form-control" name="sex" id="sex">
                    <option value="">[ Seleccione ]</option>
                    <option value="M" <?php if($sex=="M"){ echo "selected"; } ?>>Masculino</option>
                    <option value="F" <?php if($sex=="F"){ echo "selected"; } ?>>Femenino</option>
					</select>
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Direcci&oacute;n</label>
                <div class="col-sm-8">
                	<input type="text" name="dir" id="dir" class="form-control" maxlength="200" value="<?php echo $dir; ?>">
				</div>
			</div>
            
            <div class="form-group">
	            <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-8">
                	<input type="password" name="cla" id="cla" class="form-control" maxlength="200" value="<?php echo $cla; ?>">
				</div>
			</div>
		</form>
        
        <div class="panel-footer">
        	<div class="row">
            	<div class="col-sm-8 col-sm-offset-2">
                	<button class="btn-primary btn" onclick="grabar()">Guardar</button>
                    <button class="btn-default btn" onclick="cancelar()">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function grabar(){
	var tip = $("select#tip").val();
	var aul = $("select#aul").val();
	var nom = $("input#nom").val();
	var ape = $("input#ape").val();
	var dni = $("input#dni").val();
	var ema = $("input#ema").val();
	var cel = $("input#cel").val();
	var sex = $("select#sex").val();
	var dir = $("input#dir").val();
	var cla = $("input#cla").val();
	
	$("select#tip").css('border-color', '');
	$("select#aul").css('border-color', '');
	$("input#nom").css('border-color', '');
	$("input#ape").css('border-color', '');
	$("input#dni").css('border-color', '');
	$("input#ema").css('border-color', '');
	$("input#cel").css('border-color', '');
	$("select#sex").css('border-color', '');
	$("input#dir").css('border-color', '');
	$("input#cla").css('border-color', '');
	if(tip.length==0){
		$("select#tip").css('border-color', '#F00');
		$("select#tip").focus();
		return;
	}
	if(aul.length==0){
		$("select#aul").css('border-color', '#F00');
		$("select#aul").focus();
		return;
	}
	if(nom.length==0){
		$("input#nom").css('border-color', '#F00');
		$("input#nom").focus();
		return;
	}
	if(ape.length==0){
		$("input#ape").css('border-color', '#F00');
		$("input#ape").focus();
		return;
	}
	if(dni.length==0){
		$("input#dni").css('border-color', '#F00');
		$("input#dni").focus();
		return;
	}
	if(ema.length==0){
		$("input#ema").css('border-color', '#F00');
		$("input#ema").focus();
		return;
	}
	if(cel.length==0){
		$("input#cel").css('border-color', '#F00');
		$("input#cel").focus();
		return;
	}
	if(sex.length==0){
		$("select#sex").css('border-color', '#F00');
		$("select#sex").focus();
		return;
	}
	if(dir.length==0){
		$("input#dir").css('border-color', '#F00');
		$("input#dir").focus();
		return;
	}
	if(cla.length==0){
		$("input#cla").css('border-color', '#F00');
		$("input#cla").focus();
		return;
	}
	<?php if($c_tipo=="2" && $modo=="add"){ ?>
	if(parseInt(<?php echo $num_usu+1; ?>)>parseInt(<?php echo $canusu; ?>)){
		alert("Aula se encuentra llena, consulte con el admin.");
		return;
	}
	<?php } ?>

	document.frm.submit();
}

function cancelar(){
	//alert("A donde voy al cancelar.");
}
</script>
<!-- ENDFORM -->