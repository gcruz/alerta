<?php
	include("../sesion.php");
	include("../bd/bd.php");
	include("../bd/usuario.php");
	include("../bd/aula.php");
	include("../inc/funcion.php");
	$obj_bd = new BD();
	$obj_usuario = new usuario();
	$obj_aula = new aula();
	
	$modo = "pupd";

	$id      = $c_id;
	$num_usu = $obj_usuario->editar("usu_id=$id");
	$tip = $obj_usuario->e_tip();
	$aul = $obj_usuario->e_aul();
	$nom = $obj_usuario->e_nom();
	$ape = $obj_usuario->e_ape();
	$dni = $obj_usuario->e_dni();
	$ema = $obj_usuario->e_ema();
	$cel = $obj_usuario->e_cel();
	$sex = $obj_usuario->e_sex();
	$dir = $obj_usuario->e_dir();
	$cla = desencriptar($obj_usuario->e_cla(), "alerta");
	
	$w = "";
	if($c_tipo=="1"){
		$w = "1=1 and ";
	}elseif($c_tipo=="2"){
		$w = "aul_id=$c_aula and ";
	}
	$res_aul = $obj_aula->consultar("aul_id, aul_nom, aul_canusu", "$w aul_est='1'", "aul_nom");
	$num_aul = $obj_bd->num_rows($res_aul);
	
	$w = "";
	if($c_tipo=="1"){
		$w = "usu_id<>$c_id and ";
	}elseif($c_tipo=="2"){
		$w = "usu_id<>$c_id and usu_aul=$c_aula and ";
	}
	$res_usu = $obj_usuario->consultar("usu_id, usu_nom, usu_ape, usu_dni, usu_ema, usu_est", "$w 1=1", "usu_nom, usu_ape");
	$num_usu = $obj_bd->num_rows($res_usu);
?>
<!--FORM-->
<div class="panel panel-default" data-widget-editbutton="false">
	<div class="panel-heading">
		<h2>Editar Perfil</h2>
	</div>
    <div class="panel-body">
    	<form action="" class="form-horizontal row-border" name="frm1" id="frm1">
        <input type="hidden" name="modo" id="modo" value="<?php echo $modo; ?>" class="form-control">
        <input type="hidden" name="id" id="id" value="<?php echo $id; ?>" class="form-control">
			<div class="form-group">
            	<label class="col-sm-2 control-label">Perfil</label>
                <div class="col-sm-8">
                	<select class="form-control" name="ptip" id="ptip">
					<?php if($c_tipo=="1"){ ?><option value="1">Administrador</option><?php } ?>
                    <?php if($c_tipo=="2"){ ?><option value="2">Tutor</option><?php } ?>
					<?php if($c_tipo=="3"){ ?><option value="3">Padre de Familia</option><?php } ?>
                    </select>
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Aula</label>
                <div class="col-sm-8">
                	<select class="form-control" name="paul" id="paul">
                    <option value="">[ Seleccione ]</option>
                    <?php
						$canusu = 0;
						if($num_aul>0){
							while($f=$obj_bd->fetch_assoc($res_aul)){
								$canusu = $f["aul_canusu"];
								if($f["aul_id"]==$aul){ $s="selected"; }else{ $s=""; }
                    ?>
			                    <option value="<?php echo $f["aul_id"]; ?>" <?php echo $s; ?>><?php echo $f["aul_nom"]; ?></option>
                    <?php
							}
						}
                    ?>
                    </select>
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Nombres</label>
                <div class="col-sm-8">
                	<input type="text" name="pnom" id="pnom" class="form-control" maxlength="50" value="<?php echo $nom; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Apellidos</label>
                <div class="col-sm-8">
                	<input type="text" name="pape" id="pape" class="form-control" maxlength="100" value="<?php echo $ape; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">DNI</label>
                <div class="col-sm-8">
                	<input type="text" name="pdni" id="pdni" class="form-control" maxlength="8" value="<?php echo $dni; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-8">
                	<input type="text" name="pema" id="pema" class="form-control" maxlength="50" value="<?php echo $ema; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Celular</label>
                <div class="col-sm-8">
                	<input type="text" name="pcel" id="pcel" class="form-control" maxlength="50" value="<?php echo $cel; ?>">
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">G&eacute;nero</label>
                <div class="col-sm-8">
                	<select class="form-control" name="psex" id="psex">
                    <option value="">[ Seleccione ]</option>
                    <option value="M" <?php if($sex=="M"){ echo "selected"; } ?>>Masculino</option>
                    <option value="F" <?php if($sex=="F"){ echo "selected"; } ?>>Femenino</option>
					</select>
				</div>
			</div>
            
            <div class="form-group">
            	<label class="col-sm-2 control-label">Direcci&oacute;n</label>
                <div class="col-sm-8">
                	<input type="text" name="pdir" id="pdir" class="form-control" maxlength="200" value="<?php echo $dir; ?>">
				</div>
			</div>
            
            <div class="form-group">
	            <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-8">
                	<input type="password" name="pcla" id="pcla" class="form-control" maxlength="200" value="<?php echo $cla; ?>">
				</div>
			</div>
		</form>
        
        <div class="panel-footer">
        	<div class="row">
            	<div class="col-sm-8 col-sm-offset-2">
                	<button class="btn-primary btn" onclick="pgrabar()">Guardar</button>
                    <button class="btn-default btn" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function pgrabar(){
	var tip = $("select#ptip").val();
	var aul = $("select#paul").val();
	var nom = $("input#pnom").val();
	var ape = $("input#pape").val();
	var dni = $("input#pdni").val();
	var ema = $("input#pema").val();
	var cel = $("input#pcel").val();
	var sex = $("select#psex").val();
	var dir = $("input#pdir").val();
	var cla = $("input#pcla").val();
	
	$("select#ptip").css('border-color', '');
	$("select#paul").css('border-color', '');
	$("input#pnom").css('border-color', '');
	$("input#pape").css('border-color', '');
	$("input#pdni").css('border-color', '');
	$("input#pema").css('border-color', '');
	$("input#pcel").css('border-color', '');
	$("select#psex").css('border-color', '');
	$("input#pdir").css('border-color', '');
	$("input#pcla").css('border-color', '');
	if(tip.length==0){
		$("select#ptip").css('border-color', '#F00');
		$("select#ptip").focus();
		return;
	}
	if(aul.length==0){
		$("select#paul").css('border-color', '#F00');
		$("select#paul").focus();
		return;
	}
	if(nom.length==0){
		$("input#pnom").css('border-color', '#F00');
		$("input#pnom").focus();
		return;
	}
	if(ape.length==0){
		$("input#pape").css('border-color', '#F00');
		$("input#pape").focus();
		return;
	}
	if(dni.length==0){
		$("input#pdni").css('border-color', '#F00');
		$("input#pdni").focus();
		return;
	}
	if(ema.length==0){
		$("input#pema").css('border-color', '#F00');
		$("input#pema").focus();
		return;
	}
	if(cel.length==0){
		$("input#pcel").css('border-color', '#F00');
		$("input#pcel").focus();
		return;
	}
	if(sex.length==0){
		$("select#psex").css('border-color', '#F00');
		$("select#psex").focus();
		return;
	}
	if(dir.length==0){
		$("input#pdir").css('border-color', '#F00');
		$("input#pdir").focus();
		return;
	}
	if(cla.length==0){
		$("input#pcla").css('border-color', '#F00');
		$("input#pcla").focus();
		return;
	}

	$.ajax({
		type: "POST",
		data: "id=<?php echo $id; ?>&tip="+tip+"&aul="+aul+"&nom="+nom+"&ape="+ape+"&dni="+dni+"&ema="+ema+"&cel="+cel+"&sex="+sex+"&dir="+dir+"&cla="+cla+"&modo=pupd",
		url: "control/usuario.php",
		success: function(respuesta){
			alert(respuesta);
		}
	});
}
</script>
<!-- ENDFORM -->