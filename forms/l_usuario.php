<?php
	//include("../sesion.php");
	//include("bd/bd.php");
	//include("bd/aula.php");
	//include("inc/funcion.php");
	//$obj_bd = new BD();
	//$obj_aula = new aula();
	
	$w = "";
	if($c_tipo=="1"){
		$w = "usu_id<>$c_id and ";
	}elseif($c_tipo=="2"){
		$w = "usu_id<>$c_id and usu_aul=$c_aula and ";
	}
	
	$res_usu = $obj_usuario->consultar("usu_id, usu_nom, usu_ape, usu_dni, usu_ema, usu_est", "$w 1=1", "usu_nom, usu_ape");
	$num_usu = $obj_bd->num_rows($res_usu);
?>
<!--LISTADO-->
<div class="row">
	<div class="col-xs-12">
    	<div class="panel panel-default">
        	<div class="panel-heading"><h2>LISTA DE USUARIOS</h2></div>
            <div class="panel-body panel-no-padding">
            	<table id="example" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                	<tr>
                    	<th align="center">#</th>
						<th align="center">DNI</th>
						<th align="center">Nombre y Apellidos</th>
						<th align="center">Email</th>
						<th align="center">Estado</th>
						<th style="text-align: center;">Acciones</th>
					</tr>
				</thead>
				<tbody>
                <?php
					$item = 0;
					if($num_usu>0){
						while($f=$obj_bd->fetch_assoc($res_usu)){
							$item++;
							if($f["usu_est"]=="1"){
								$est = "success";
								$act = "A";
							}else{
								$est = "danger";
								$act = "D";
							}
				?>
				
							<tr>
								<td align="center"><?php echo $item; ?></td>
								<td align="center"><?php echo $f["usu_dni"]; ?></td>
								<td align="left"><?php echo mayuscula($f["usu_nom"]." ".$f["usu_ape"]); ?></td>
								<td align="left"><?php echo minuscula($f["usu_ema"]); ?></td>
								<td align="center"><span id="activo<?php echo $f["usu_id"]; ?>" lang="<?php echo $act; ?>" class="label label-<?php echo $est; ?>" onclick="estado('<?php echo $f["usu_id"]; ?>')" style="cursor:pointer"><?php echo $act; ?></span></td>
								<td style="text-align: center;"><a href="home.php?php=usuario&modo=upd&id=<?php echo $f["usu_id"]; ?>" class="btn btn-default btn-xs">Ver</a></td>
							</tr>
				<?php
						}
					}
				?>
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
function estado(id){
	var lan = $("#activo"+id).attr('lang');
	var act = "";
	var cla = "";
	var est = "";
	if(lan=="A"){ est="0"; act="D"; cla="danger"; }else{ est="1"; act="A"; cla="success"; }
	$.ajax({
		type: "POST",
		data: "id="+id+"&est="+est+"&modo=del",
		url: "control/usuario.php",
		success: function(respuesta){
			$("#activo"+id).attr('lang', act);
			$("#activo"+id).html(act);
			$("#activo"+id).removeClass();
			$("#activo"+id).addClass('label label-'+cla);
		}
	});
}
</script>
<!--END LISTADO-->