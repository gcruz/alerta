<?php
	include("../sesion.php");
	include("../bd/bd.php");
	include("../bd/usuario.php");
	include("../inc/funcion.php");
	$obj_bd = new BD();
	$obj_usuario = new usuario();
	
	$w = "";
	if($c_tipo=="2"){
		$w = "usu_id<>$c_id and usu_aul=$c_aula and ";
	}
	
	$res_usu = $obj_usuario->consultar("usu_id, usu_nom, usu_ape", "$w usu_est='1'", "usu_nom, usu_ape");
	$num_usu = $obj_bd->num_rows($res_usu);
?>
<!--FORM-->
<div class="panel panel-default" data-widget-editbutton="false">
	<div class="panel-heading">
		<h2>Nueva Alerta</h2>
	</div>
	<div class="panel-body">
    	<form class="form-horizontal row-border" name="frm" id="frm">
        <input type="hidden" name="modo" id="modo" value="add" class="form-control">
        	<div class="form-group">
            	<label class="col-sm-2 control-label">Asunto</label>
				<div class="col-sm-8">
					<input type="text" name="asu" id="asu" class="form-control">
				</div>
			</div>
            
            <div class="form-group">
				<label class="col-sm-2 control-label">Destinatario</label>
				<div class="col-sm-8">
					<!--select class="form-control" name="des" id="des"-->
                    <select class="form-control chosen-select" name="des" id="des" style="width:240px !important" multiple="multiple" data-placeholder="Seleccione">
					<option value="T">Todos</option>
                    <?php
						if($num_usu>0){
							while($f=$obj_bd->fetch_assoc($res_usu)){
                    ?>
			                    <option value="<?php echo $f["usu_id"]; ?>"><?php echo $f["usu_nom"]." ".$f["usu_ape"]; ?></option>
                    <?php
							}
						}
                    ?>
					</select>
                    <style>
					.chosen-container{
						width: 400px !important;
					}
					</style>
                	<script type="text/javascript">
						$(".chosen-select").chosen(); 
					</script>
				</div>
			</div>
                
            <div class="form-group">
				<label class="col-sm-2 control-label">Prioridad</label>
				<div class="col-sm-8">
					<label class="radio-inline icheck">
						<input type="radio" name="pri" id="pri" value="1"> Comunicado
					</label>
					<label class="radio-inline icheck">
						<input type="radio" name="pri" id="pri" value="2"> Citación
					</label>
					<label class="radio-inline icheck">
						<input type="radio" name="pri" id="pri" value="3"> Alerta
					</label>
				</div>
			</div>
                
			<div class="form-group">
				<label class="col-sm-2 control-label">Mensaje</label>
				<div class="col-sm-8">
					<textarea class="form-control" name="men" id="men"></textarea>
				</div>
			</div>
		</form>
        
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div id="btn_w" style="display:none" >
						<i class="fa fa-fw fa-refresh load_anim"></i> Enviando, por favor espere...
					</div>
					<button id="btn_e" class="btn-primary btn" onclick="enviar()">Enviar</button>
					<button id="btn_c" class="btn-default btn" data-dismiss="modal">Cancelar</button>
				</div>
            </div>
		</div>
	</div>
</div>
<script>
function enviar(){
	var asu = $("input#asu").val();
	var des = $("select#des").val();
	var pri = $("input[name='pri']:checked").val();
	var men = $("textarea#men").val();
	
	$("input#asu").css('border-color', '');
	$("select#des").css('border-color', '');
	$("textarea#men").css('border-color', '');
	if(asu.length==0){
		$("input#asu").css('border-color', '#F00');
		$("input#asu").focus();
		return;
	}
	if(des!==null){
	}else{
		alert("Seleccione destinatario.");
		return;
	}
	if(pri){
	}else{
		alert("Seleccione una prioridad.");
		return;
	}
	if(men.length==0){
		$("textarea#men").css('border-color', '#F00');
		$("textarea#men").focus();
		return;
	}
	
	$("#btn_e, #btn_c").hide();
	$("#btn_w").show();

	$.ajax({
		type: "POST",
		data: "id=&asu="+asu+"&des="+des+"&pri="+pri+"&men="+men+"&modo=add",
		url: "control/alerta.php",
		success: function(respuesta){
			//alert(respuesta);
			if(respuesta=="G"){
				alert("Alerta enviado correctamente.");
				$("input#asu").val('');
				$(".chosen-select").val('').trigger("chosen:updated");
				$("input:radio").attr("checked", false);
				$("textarea#men").val('');
				$("#btn_e, #btn_c").show();
				$("#btn_w").hide();
			}
		}
	});
}

function cancelar(){
	alert("cancelar");
}
</script>
<!-- ENDFORM -->