<?php
	//include("../sesion.php");
	//include("bd/bd.php");
	//include("bd/aula.php");
	//include("inc/funcion.php");
	//$obj_bd = new BD();
	//$obj_aula = new aula();
	
	$res_aul = $obj_aula->consultar("aul_id, aul_nom, aul_insedu, aul_codmod, aul_canusu, aul_est", "1=1", "aul_nom");
	$num_aul = $obj_bd->num_rows($res_aul);
?>
<!--LISTADO-->
<div class="row">
	<div class="col-xs-12">
    	<div class="panel panel-default">
        	<div class="panel-heading"><h2>LISTA DE AULAS</h2></div>
            <div class="panel-body panel-no-padding">
            	<table id="example" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                	<tr>
                    	<th align="center">#</th>
						<th align="center">C.Modular</th>
						<th align="center">Nombre</th>
						<th align="center">I.Educativa</th>
						<th align="center">C.Usuarios</th>
						<th align="center">Estado</th>
						<th style="text-align: center;">Acciones</th>
					</tr>
				</thead>
				<tbody>
                <?php
					$item = 0;
					if($num_aul>0){
						while($f=$obj_bd->fetch_assoc($res_aul)){
							$item++;
							if($f["aul_est"]=="1"){
								$est = "success";
								$act = "A";
							}else{
								$est = "danger";
								$act = "D";
							}
				?>
				
						<tr>
							<td align="center"><?php echo $item; ?></td>
							<td align="center"><?php echo $f["aul_codmod"]; ?></td>
							<td align="left"><?php echo mayuscula($f["aul_nom"]); ?></td>
							<td align="left"><?php echo mayuscula($f["aul_insedu"]); ?></td>
							<td align="center"><?php echo $f["aul_canusu"]; ?></td>
							<td align="center"><span id="activo<?php echo $f["aul_id"]; ?>" lang="<?php echo $act; ?>" class="label label-<?php echo $est; ?>" onclick="estado('<?php echo $f["aul_id"]; ?>')" style="cursor:pointer"><?php echo $act; ?></span></td>
							<td style="text-align: center;"><a href="home.php?php=aula&modo=upd&id=<?php echo $f["aul_id"]; ?>" class="btn btn-default btn-xs">Ver</a></td>
						</tr>
				<?php
						}
					}
				?>
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
function estado(id){
	var lan = $("#activo"+id).attr('lang');
	var act = "";
	var cla = "";
	var est = "";
	if(lan=="A"){ est="0"; act="D"; cla="danger"; }else{ est="1"; act="A"; cla="success"; }
	$.ajax({
		type: "POST",
		data: "id="+id+"&est="+est+"&modo=del",
		url: "control/aula.php",
		success: function(respuesta){
			$("#activo"+id).attr('lang', act);
			$("#activo"+id).html(act);
			$("#activo"+id).removeClass();
			$("#activo"+id).addClass('label label-'+cla);
		}
	});
}
</script>
<!--END LISTADO-->