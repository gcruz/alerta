<?php
	include("../bd/bd.php");
	include("../bd/usuario.php");
	include("../inc/funcion.php");
	$obj_bd = new BD();
	$obj_usuario = new usuario();

	$id = $_REQUEST["id"];
	$aula = $_REQUEST["aula"];
	$array= array();

	$res_usu = $obj_usuario->consultar("usu_id, usu_nom, usu_ape", "usu_id<>$id and usu_aul=$aula and usu_est='1'", "usu_nom, usu_ape");
	$num_usu = $obj_bd->num_rows($res_usu);
	
	if($num_usu>0){
		$array[] = array('value' => 'T', 'name' => 'Todos');
		while($f=$obj_bd->fetch_assoc($res_usu)){
			$usu = $f["usu_id"];
			$nomape = $f["usu_nom"]." ".$f["usu_ape"];
			$array[] = array('value' => "$usu", 'name' => "$nomape");
		}
	}

	echo json_encode($array);
?>