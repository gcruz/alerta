<?php session_start();

	if(!isset($_SESSION['s_id']) || !isset($_SESSION['s_tipo']) || !isset($_SESSION['s_apenom']) || !isset($_SESSION['s_aula'])){

		echo "<script>location.href='index.php';</script>";

	}



	$c_id     = $_SESSION['s_id'];

	$c_tipo   = $_SESSION['s_tipo'];

	$c_apenom = mb_convert_case(trim($_SESSION['s_apenom']), MB_CASE_TITLE, "UTF-8");

	$c_aula   = $_SESSION['s_aula'];

	$aul      = $_SESSION['s_aula'];

	

	if($c_tipo=="1"){

		$ico_aula    = "1";

		$ico_usuario = "1";

		$ico_buscar  = "0";

		$ico_alerta  = "0";

		$ult_alerta  = "0";

		$lis_alerta  = "0";

		$mis_alerta  = "0";

	}elseif($c_tipo=="2"){

		$ico_aula    = "0";

		$ico_usuario = "1";

		$ico_buscar  = "1";

		$ico_alerta  = "1";

		$ult_alerta  = "0";

		$lis_alerta  = "0";

		$mis_alerta  = "1";

	}elseif($c_tipo=="3"){

		$ico_aula    = "0";

		$ico_usuario = "0";

		$ico_buscar  = "1";

		$ico_alerta  = "0";

		$ult_alerta  = "1";

		$lis_alerta  = "1";

		$mis_alerta  = "0";

	}else{

		$ico_aula    = "1";

		$ico_usuario = "1";

		$ico_buscar  = "1";

		$ico_alerta  = "1";

		$ult_alerta  = "1";

		$lis_alerta  = "1";

		$mis_alerta  = "1";

	}

?>

<?php //include("sesion.php"); ?>

<?php

	include("bd/bd.php");

	include("bd/aula.php");

	include("bd/usuario.php");

	include("bd/alerta.php");

	include("inc/funcion.php");

	$obj_bd = new BD();

	$obj_aula = new aula();

	$obj_usuario = new usuario();

	$obj_alerta = new alerta();



	if(isset($_REQUEST['php'])){

		$php = $_REQUEST['php'];

		$php1 = "n_".$_REQUEST['php'];

		$php2 = "l_".$_REQUEST['php'];

	}else{

		if($c_tipo=="1"){

			$php1 = "n_usuario";

			$php2 = "l_usuario";

		}elseif($c_tipo=="2"){

			$php1 = "n_usuario";

			$php2 = "l_usuario";

		}elseif($c_tipo=="3"){

			$php1 = "";

			$php2 = "";

		}

	}

?>

<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

	

    <title>Alerta - Admin</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">

	<meta http-equiv="Expires" content="0" />

	<meta http-equiv="Pragma" content="no-cache" />



	<head>

    <link rel="shortcut icon" href="img/ico.png" type="image/png">

    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,700" rel="stylesheet" type="text/css">

    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="css/admin.css" type="text/css" rel="stylesheet"><!-- Todos los estilos bravos -->



<link rel="stylesheet" href="css/chosen.css" />



<script type="text/javascript">

if(history.forward(1)){

	location.replace( history.forward(1) );

}

function buscar(p){

	var bus = $("input#txbuscar").val();

	location.href = "home.php?php="+p+"&bus="+bus+"&b=1";

}

</script>

	</head>



    <body class="infobar-offcanvas" style="">

      

        

	<header id="topnav" class="navbar navbar-inverse navbar-fixed-top clearfix" role="banner">



	

	<a class="navbar-brand" href="home.php">Alerta</a>





	<ul class="nav navbar-nav toolbar pull-right">

		<?php if($mis_alerta=="1"){ ?>

		<li class="dropdown toolbar-icon-bg hidden-xs">

			<a href="home.php?php=misalerta" class="hasnotifications dropdown-toggle" <?php if($desplegable==1){ echo 'data-toggle="dropdown"'; } ?> aria-expanded="false"><span class="icon-bg"><i class="fa fa-fw fa-tasks"></i></span></a>

		</li>

		<?php } ?>



		<?php if($ico_aula=="1"){ ?>

		<li class="dropdown toolbar-icon-bg hidden-xs">

			<a href="home.php?php=aula" class="hasnotifications dropdown-toggle" <?php if($desplegable==1){ echo 'data-toggle="dropdown"'; } ?> aria-expanded="false"><span class="icon-bg"><i class="fa fa-fw fa-tasks"></i></span></a>

		</li>

		<?php } ?>



		<?php if($ico_usuario=="1"){ ?>

		<li class="dropdown toolbar-icon-bg hidden-xs">

			<a href="home.php?php=usuario" class="hasnotifications dropdown-toggle" <?php if($desplegable==1){ echo 'data-toggle="dropdown"'; } ?> aria-expanded="false"><span class="icon-bg"><i class="fa fa-fw fa-user"></i></span></a>

		</li>

		<?php } ?>



		<?php if($ico_alerta=="1"){ ?>

        			

        	<li class="dropdown toolbar-icon-bg">

			<a href="forms/n_alerta.php" data-target="#myModal" data-toggle="modal" class="hasnotifications dropdown-toggle" <?php if($desplegable==1){ echo 'data-toggle="dropdown"'; } ?> aria-expanded="false">

            	<span class="icon-bg"><i class="fa fa-fw fa-bell"></i></span><!-- <span class="badge badge-alizarin">5</span>-->

            </a>

			<?php if($ult_alerta=="1"){ ?>

			<div class="dropdown-menu notifications arrow">

				<div class="dd-header">

					<span>Alertas</span>

					<span><a href="#">Novedades</a></span>

				</div>

				<ul class="scrollthis">

					<li class="">

						<a href="#" class="notification-success">

							<div class="notification-icon"><i class="fa fa-check fa-fw"></i></div>

							<div class="notification-content"><strong>Lorem ipsum</strong> dolor sit amet consectetur adipisicing elit!</div>

							<div class="notification-time">40m</div>

						</a>

					</li>

					<li class="">

						<a href="#" class="notification-danger">

							<div class="notification-icon"><i class="fa fa-times fa-fw"></i></div>

							<div class="notification-content">Etiam porta sem malesuada</div>

							<div class="notification-time">5h</div>

						</a>

					</li>

					<li class="">

						<a href="#" class="notification-inverse">

							<div class="notification-icon"><i class="fa fa-cloud fa-fw"></i></div>

							<div class="notification-content"><strong>Nesciunt</strong> reprehenderit provident distinctio eveniet cupiditate atque</div>

							<div class="notification-time">16h</div>

						</a>

					</li>

					<li class="">

						<a href="#" class="notification-warning">

							<div class="notification-icon"><i class="fa fa-warning fa-fw"></i></div>

							<div class="notification-content">Aperiam accusamus modi ipsum officia quas nisi!</div>

							<div class="notification-time">1d</div>

						</a>

					</li>

					<li class="">

						<a href="#" class="notification-info">

							<div class="notification-icon"><i class="fa fa-shopping-cart fa-fw"></i></div>

							<div class="notification-content">Libero distinctio eveniet</div>

							<div class="notification-time">5d</div>

						</a>

					</li>

					<li class="">

						<a href="#" class="notification-primary">

							<div class="notification-icon"><i class="fa fa-comment fa-fw"></i></div>

							<div class="notification-content">Officiis modi ipsum officia ad dolor sit amet consectetur sit voluptatem accusantium doloremque laudantium totam rem aperiam</div>

							<div class="notification-time">8d</div>

						</a>

					</li>

				</ul>

				<div class="dd-footer">

					<a href="#">Ver todas las Alertas</a>

				</div>

			</div>

			<?php } ?>

		</li>

		<?php } ?>



		<?php if($ico_buscar=="1"){ ?>

		<li class="dropdown toolbar-icon-bg demo-search-hidden mr5">

			<a href="#" class="dropdown-toggle tooltips" data-toggle="dropdown" data-original-title="" title="" aria-expanded="false"><span class="icon-bg"><i class="fa fa-fw fa-search"></i></span></a>

			<div class="dropdown-menu arrow search dropdown-menu-form">

				<div class="dd-header">

					<span>Buscar</span>

					<span><a href="#">Busqueda Avanzada</a></span>

				</div>

				<div class="input-group">

					<input type="text" name="txbuscar" id="txbuscar" class="form-control" placeholder="">

					<span class="input-group-btn">

						<a class="btn btn-primary" href="javascript:void(0)" onclick="buscar('<?php if($mis_alerta=="1"){ echo "misalerta"; }else{ echo ""; } ?>')">Buscar</a>

					</span>

				</div>

			</div>

		</li>

		<?php } ?>



		<li class="dropdown">

			<a href="#" class="dropdown-toggle username" data-toggle="dropdown" aria-expanded="false">

				<span class="hidden-xs"><?php echo $c_apenom; ?></span>

				<img class="img-circle" src="http://www.elmostrador.cl/media/2011/04/Chomali_50x50.jpg" alt="Dangerfield">



			</a>

			<ul class="dropdown-menu userinfo">

				<li>

					<a href="forms/n_perfil.php" data-target="#myModal" data-toggle="modal" class="hasnotifications dropdown-toggle" aria-expanded="false">

						<span class="pull-left">Editar Perfil</span>

						<i class="pull-right fa fa-pencil"></i>

					</a>

				</li>

				

				<li><a href="#"><span class="pull-left">Configuración</span> <i class="pull-right fa fa-cogs"></i></a></li>

				<li><a href="#"><span class="pull-left">Ayuda</span> <i class="pull-right fa fa-question-circle"></i></a></li>

				<li class="divider"></li>

				<li><a href="#"><span class="pull-left">Ganancias</span> <i class="pull-right fa fa-money"></i></a></li>

				<li><a href="#"><span class="pull-left">Manuales</span> <i class="pull-right fa fa-file-o"></i></a></li>

				<li class="divider"></li>

				<li><a href="cerrar.php"><span class="pull-left">Salir</span> <i class="pull-right fa fa-sign-out"></i></a></li>

			</ul>

		</li>



	</ul>



</header>



<div id="wrapper">

                

	<div class="static-content-wrapper">

		<div class="static-content">

			<div class="page-content">

				<div class="page-heading">

					<?php if($c_tipo=="1"){ ?><h1>Panel de Administración</h1><?php } ?>

					<?php if($c_tipo=="2"){ ?><?php } ?>

					<?php if($c_tipo=="3"){ ?><h1>Mis Alertas</h1><?php } ?>

                </div>

                <div class="container-fluid">

                                

				<div id="panel-advancedoptions">



		<?php if($lis_alerta=="1" || $mis_alerta=="1"){ ?>

                    <div class="row">

			<?php

				$b=0;

				$bus="";

				$w="";

				if(isset($_REQUEST['b'])){

					$b=$_REQUEST['b'];

					$bus=$_REQUEST['bus'];

					$w="and (a.ale_asu like '%$bus%' || a.ale_men like '%$bus%')";

				}

				$num_ale = 0;

				if($php=="misalerta"){

					$res_ale = $obj_bd->consulta("select a.ale_id, a.ale_asu, a.ale_des, a.ale_pri, a.ale_men, a.ale_fec, a.usu_id from alerta a inner join usuario u on a.usu_id=u.usu_id where a.usu_id=$c_id and a.ale_est='1' and u.usu_aul='$c_aula' $w order by ale_id desc");

					$num_ale = $obj_bd->num_rows($res_ale);

				}elseif($lis_alerta=="1"){

					$res_ale = $obj_bd->consulta("select a.ale_id, a.ale_asu, a.ale_des, a.ale_pri, a.ale_men, a.ale_fec, a.usu_id from alerta a inner join usuario u on a.usu_id=u.usu_id where (a.ale_des=',T,' || a.ale_des like '%,$c_id,%') and ale_est='1' and u.usu_aul='$c_aula' $w order by ale_id desc");

					$num_ale = $obj_bd->num_rows($res_ale);

				}

				if($num_ale>0){

					while($f=$obj_bd->fetch_assoc($res_ale)){

						$num_usu = $obj_usuario->editar("usu_id=".$f["usu_id"]);

						$nomape  = $obj_usuario->e_nom()." ".$obj_usuario->e_ape();

			?>

                        <div class="col-md-3">

                            <a style="cursor:default" class="info-tiles tiles-inverse has-footer color_<?php echo $f["ale_pri"]?>" href="#">

                                <div class="tiles-heading">

                                    <div class="pull-left"><?php echo $f["ale_asu"]; ?></div>

                                    <div class="pull-right">

                                        <div id="tileorders" class="sparkline-block"><canvas width="39" height="13" style="display: inline-block; width: 39px; height: 13px; vertical-align: top;"></canvas></div>

                                    </div>

                                </div>

                                <div class="tiles-body">

                                    <div class="text-center"><?php echo $f["ale_men"]; ?></div>

                                </div>

                                <div class="tiles-footer">

                                    <div class="pull-left">Tutor: <?php echo $nomape; ?></div>

                                    <div class="pull-right percent-change"><?php echo formato_fecha($f["ale_fec"], 1); ?></div>

                                </div>

                            </a>

                        </div>

			<?php

					}

				}

			?>



                    </div> <!-- FIN ROW DE ALERTAS -->

		<?php } ?>

                	

                    <!--AQUI METES FORM + LISTADO-->

                    <div class="row">

                    	<div class="col-md-6"><?php if($c_tipo=="1" || $c_tipo=="2" && $php!="misalerta"){ include("forms/".$php1.".php"); } ?></div>

                        <div class="col-md-6"><?php if($c_tipo=="1" || $c_tipo=="2" && $php!="misalerta"){ include("forms/".$php2.".php"); } ?></div>

                    </div>

                    

                    

                </div>



				</div> <!-- .container-fluid -->

			</div> <!-- #page-content -->

		</div>        

	

    </div>

</div>



<!-- Modal -->

<div class="modal fade" data-backdrop="static" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

        </div> <!-- /.modal-content -->

    </div> <!-- /.modal-dialog -->

</div> <!-- /.modal -->





<!--

<footer role="contentinfo">

	<div class="clearfix">

		<ul class="list-unstyled list-inline pull-left">

			<li><h6 style="margin: 0;"> © 2014 Alerta</h6></li>

		</ul>

	</div>

</footer>

-->





<!-- jQuery -->

<script src="js/jquery.js"></script>



<!-- Bootstrap Core JavaScript -->

<script src="js/bootstrap.min.js"></script>



<script src="js/chosen.jquery.min.js" type="text/javascript"></script>



</body>

</html>