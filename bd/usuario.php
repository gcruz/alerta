<?php
	class usuario extends BD{
		var $usu_id;
		var $usu_tip;
		var $usu_aul;
		var $usu_nom;
		var $usu_ape;
		var $usu_dni;
		var $usu_ema;
		var $usu_cel;
		var $usu_sex;
		var $usu_dir;
		var $usu_cla;
		var $usu_fec;
		var $usu_cod;
		var $usu_est;
		function grabar($tip, $aul, $nom, $ape, $dni, $ema, $cel, $sex, $dir, $cla, $fec, $cod, $est){
			$this->proceso("insert into usuario(usu_tip, usu_aul, usu_nom, usu_ape, usu_dni, usu_ema, usu_cel, usu_sex, usu_dir, usu_cla, usu_fec, usu_cod, usu_est) values($tip, $aul, '$nom', '$ape', '$dni', '$ema', '$cel', '$sex', '$dir', '$cla', $fec, $cod, '$est')");
			return mysql_insert_id();
		}

		function actualizar($id, $tip, $aul, $nom, $ape, $dni, $ema, $cel, $sex, $dir, $cla){
			$this->proceso("update usuario set usu_tip=$tip, usu_aul=$aul, usu_nom='$nom', usu_ape='$ape', usu_dni='$dni', usu_ema='$ema', usu_cel='$cel', usu_sex='$sex', usu_dir='$dir', usu_cla='$cla' where usu_id=$id");
		}

		function eliminar($id){
			$this->proceso("delete from usuario where usu_id=$id");
		}

		function editar($where='1'){
			$res = $this->consulta("select * from usuario where $where limit 1");
			$num = $this->num_rows($res);
			if($num>0){
				$this->usu_id  = $this->result($res, 0);
				$this->usu_tip = $this->result($res, 1);
				$this->usu_aul = $this->result($res, 2);
				$this->usu_nom = $this->result($res, 3);
				$this->usu_ape = $this->result($res, 4);
				$this->usu_dni = $this->result($res, 5);
				$this->usu_ema = $this->result($res, 6);
				$this->usu_cel = $this->result($res, 7);
				$this->usu_sex = $this->result($res, 8);
				$this->usu_dir = $this->result($res, 9);
				$this->usu_cla = $this->result($res, 10);
				$this->usu_fec = $this->result($res, 11);
				$this->usu_cod = $this->result($res, 12);
				$this->usu_est = $this->result($res, 13);
			}
			return $num;
		}

		function consultar($campo='*', $where='1', $order='1'){
			$res = $this->consulta("select $campo from usuario where $where order by $order");
			return $res;
		}

		function e_id(){ return $this->usu_id; }
		function e_tip(){ return $this->usu_tip; }
		function e_aul(){ return $this->usu_aul; }
		function e_nom(){ return trim($this->usu_nom); }
		function e_ape(){ return trim($this->usu_ape); }
		function e_dni(){ return trim($this->usu_dni); }
		function e_ema(){ return trim($this->usu_ema); }
		function e_cel(){ return $this->usu_cel; }
		function e_sex(){ return $this->usu_sex; }
		function e_dir(){ return trim($this->usu_dir); }
		function e_cla(){ return trim($this->usu_cla); }
		function e_fec(){ return $this->usu_fec; }
		function e_cod(){ return $this->usu_cod; }
		function e_est(){ return $this->usu_est; }
	}
?>