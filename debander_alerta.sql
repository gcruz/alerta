-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 23-11-2015 a las 22:21:51
-- Versión del servidor: 5.5.46-cll
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `debander_alerta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alerta`
--

CREATE TABLE IF NOT EXISTS `alerta` (
  `ale_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ale_asu` varchar(200) NOT NULL,
  `ale_des` varchar(200) NOT NULL COMMENT 'Tabla Usuario',
  `ale_pri` varchar(5) NOT NULL,
  `ale_men` text NOT NULL,
  `ale_hor` varchar(50) NOT NULL,
  `ale_fec` bigint(20) NOT NULL,
  `usu_id` bigint(20) NOT NULL,
  `ale_est` varchar(5) NOT NULL,
  PRIMARY KEY (`ale_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=121 ;

--
-- Volcado de datos para la tabla `alerta`
--

INSERT INTO `alerta` (`ale_id`, `ale_asu`, `ale_des`, `ale_pri`, `ale_men`, `ale_hor`, `ale_fec`, `usu_id`, `ale_est`) VALUES
(1, 'ALERTA', ',T,', '1', 'Ahora eres un PADRE ALERTA.\nGracias por confiar en nosotros.', '02:20:37', 20150406, 1, '1'),
(16, 'bien', ',T,', '1', 'Bien', '13:35:02', 20150406, 5, '1'),
(15, 'HOLA', ',10,', '3', 'BIENVENIDOS', '13:34:04', 20150406, 5, '1'),
(14, 'hola', ',3,', '3', 'hola', '13:32:55', 20150406, 5, '1'),
(13, 'eres', ',13,', '1', 'Eres ', '13:32:42', 20150406, 5, '1'),
(12, 'hola', ',T,', '3', 'bienvenidos', '13:32:37', 20150406, 5, '1'),
(11, 'eres', ',10,', '1', 'Re', '13:32:04', 20150406, 5, '1'),
(10, 'BIENVENIDOS', ',T,', '3', 'Bienvenidos', '13:29:43', 20150406, 5, '1'),
(17, 'HOLA BIENVENIDOS', ',10,3,18,', '3', 'AlERTA', '13:35:13', 20150406, 5, '1'),
(18, 'eres', ',17,18,10,19,11,12,20,2,13,3,21,14,4,15,6,16,7,8,9,', '1', 'De', '13:36:14', 20150406, 5, '1'),
(19, 'eres', ',17,18,10,19,11,12,20,2,13,3,21,14,4,15,6,16,7,8,9,', '3', 'De', '13:36:38', 20150406, 5, '1'),
(20, 'eres', ',13,', '3', 'De', '13:40:40', 20150406, 5, '1'),
(21, 'hola', ',18,', '3', 'Hi', '13:42:08', 20150406, 5, '1'),
(22, 'hola', ',10,', '3', 'Hola carmen ', '14:24:37', 20150406, 6, '1'),
(23, 'hola ', ',5,', '1', 'Ya tengo alerta ', '14:26:32', 20150406, 6, '1'),
(62, 'Estas ahi', ',3,', '3', 'Hola estas por ahi?', '10:01:11', 20150408, 6, '1'),
(63, 'HOLA ADALBERTO', ',5,', '3', 'Que tal , esto es una prueba de notificacion para IOS.', '10:04:39', 20150408, 6, '1'),
(67, 'dpto', ',6,', '3', 'Happy', '16:13:00', 20150408, 5, '1'),
(65, 'Hola Vane', ',6,', '3', 'Que tal Vane, esto es una prueba, me avisas si pasa algo.', '12:19:56', 20150408, 3, '1'),
(68, 'Prueba 2da', ',5,6,', '3', 'Prueba de tiempo 2, 2 dispositivos.', '16:15:54', 20150408, 3, '1'),
(69, 'compra', ',6,', '1', 'Bello', '16:16:44', 20150408, 5, '1'),
(70, 'aa', ',6,', '3', 'Aa', '16:22:20', 20150408, 5, '1'),
(71, 'alerta', ',6,', '3', 'Ojo sube volumen', '16:34:49', 20150408, 5, '1'),
(72, 'Dispositivo 2', ',5,6,', '3', 'Hola ALERTA', '17:58:20', 20150408, 3, '1'),
(73, 'asd', ',5,6,', '3', '123', '17:59:56', 20150408, 3, '1'),
(74, 'Estate ALERTA', ',5,', '3', 'alerta!!!!! Avisa si llega ', '19:39:45', 20150408, 6, '1'),
(75, 'vamos ', ',6,', '3', 'Vamos', '19:40:35', 20150408, 5, '1'),
(76, 'Apla llega', ',4,', '3', 'Una alerta mañanera', '09:00:28', 20150409, 3, '1'),
(77, 'Alerta por la tarde', ',3,4,', '3', 'Alerta por la tarde', '18:40:45', 20150409, 6, '1'),
(78, 'Hola test iOS', ',5,3,', '3', 'Alerta 8:47 am', '08:48:00', 20150410, 6, '1'),
(79, 'Pagar apple', ',5,', '3', 'No olvides depositarme para pagar S/. 450.00.', '21:14:50', 20150410, 3, '1'),
(80, 'no llego', ',3,', '3', 'No llego', '22:46:19', 20150410, 5, '1'),
(81, 'adiós', ',4,', '2', 'Hola mundo', '03:10:06', 20150411, 3, '1'),
(82, 'quererte', ',4,', '3', 'Usado cofia ', '03:12:58', 20150411, 3, '1'),
(83, 'mensaje', ',7,4,', '2', 'Hola', '14:43:17', 20150411, 3, '1'),
(84, 'prueba', ',4,', '1', 'El mejor regalo de cumple mi sueño de mi casa a dormir que ya me voy a ir al cine a ver si te vas de mi casa a dormir que ya me voy a ir al cine a ver si te vas de tema con la misma hora que es el que se te ve el pelo de ', '20:26:58', 20150412, 3, '1'),
(85, 'noche', ',4,', '3', 'Hola', '20:29:34', 20150412, 3, '1'),
(86, 'ALERTA', ',T,', '1', 'Ahora estas ALERTA.\nGracias por confiar en nosotros.', '02:20:37', 20150406, 3, '1'),
(87, 'HOLA MUCHACHOS', ',T,', '3', 'Esto es una prueba integral de la app, espero respuesta.', '17:52:48', 20150413, 3, '1'),
(66, 'Hola 2', ',5,', '3', 'Prueba de tiempo.', '15:58:20', 20150408, 6, '1'),
(88, 'Hola Peter', ',4,', '3', 'Una alerta', '17:56:47', 20150413, 3, '1'),
(89, 'Hola Jose', ',22,', '3', 'Jose no te asustes, esto es una prueba.', '17:58:51', 20150413, 3, '1'),
(90, 'Hola ', ',22,', '3', 'Probando', '18:07:04', 20150413, 3, '1'),
(91, 'Prueba Final', ',T,', '3', 'La prueba final, a ver si funka.', '18:09:04', 20150413, 3, '1'),
(92, '123-123-123', ',T,', '3', 'Un mensaje más.', '18:14:24', 20150413, 3, '1'),
(93, 'ALERTA', ',T,', '1', 'Ahora eres un PADRE ALERTA.\r\nGracias por confiar en nosotros.', '02:20:37', 20150406, 5, '1'),
(94, 'Asunto', ',3,', '3', 'Mensaje de prueba', '17:41:54', 20150414, 2, '1'),
(95, 'ddd', ',21,', '3', 'sdsdfdf', '14:34:39', 20150421, 2, '1'),
(96, 'tet', ',21,10,', '3', 'asdasd', '14:34:57', 20150421, 2, '1'),
(97, 'gfee', ',14,', '2', 'fgfg', '14:35:21', 20150421, 2, '1'),
(98, 'Test final', ',4,', '3', 'Mandando', '21:22:48', 20150422, 3, '1'),
(99, 'adios', ',4,', '3', 'Habla', '21:27:10', 20150422, 3, '1'),
(100, 'hgfhgfhgf', ',4,', '3', '31231', '21:28:12', 20150422, 3, '1'),
(101, '123', ',6,', '2', '123', '21:38:50', 20150422, 3, '1'),
(102, 'Luis', ',4,', '2', 'Luis', '21:53:50', 20150422, 3, '1'),
(103, 'testing', ',4,', '2', 'una citacion', '12:06:21', 20150423, 3, '1'),
(104, 'mensaje', ',4,', '2', 'Hola', '21:15:07', 20150423, 3, '1'),
(105, 'hola', ',4,', '2', 'Prueba uno', '14:04:43', 20150505, 3, '1'),
(106, 'hola', ',24,', '1', 'Hola hugo', '16:33:45', 20150609, 5, '1'),
(107, 'hola', ',24,', '3', 'Hola', '16:34:26', 20150609, 5, '1'),
(108, 'Eureka', ',,', '3', 'Eureka', '16:41:00', 20150609, 5, '1'),
(109, 'Hola', ',,', '3', 'Hola', '16:41:44', 20150609, 5, '1'),
(110, 'hola mensaje', ',,', '2', 'Hola a todos ', '08:40:37', 20150620, 3, '1'),
(111, 'peru', ',,', '3', 'Hola gente mañana ganamos si o si arriba Perú', '08:44:20', 20150620, 3, '1'),
(112, 'Bienvenido', ',T,', '3', 'Ahora estas ALERTA.\nGracias por confiar en nosotros.', '11:25:17', 20150702, 33, '1'),
(113, 'Asunto', ',3,', '3', 'Mensaje de prueba', '15:57:08', 20150707, 2, '1'),
(114, 'Asunto', ',3,', '3', 'Mensaje de prueba', '15:57:58', 20150707, 3, '1'),
(115, 'Asunto', ',3,', '3', 'Mensaje de prueba', '15:58:22', 20150707, 2, '1'),
(116, 'Asunto', ',3,', '3', 'Mensaje de prueba', '15:58:26', 20150707, 2, '1'),
(117, 'Asunto', ',3,', '3', 'Mensaje de prueba', '15:58:29', 20150707, 2, '1'),
(118, 'Luis Felipe marca Zapata', ',4,', '2', 'Judía rvtrvrvvrvtvbtbtbrv que te gusta que le da a conocer la historia y me dijo mi abuela se fue y te digo la palabra es oiooooooooooooooo y me dijo mi abuela se fue y te digo la palabra es un gran amigo que te gusta que le da a conocer la historia y me ', '20:28:36', 20150816, 3, '1'),
(119, 's', ',22,4,23,', '1', 's', '13:41:39', 20150930, 3, '1'),
(120, 'Buen fin de semana', ',29,', '3', 'Buen fin de semana', '12:20:24', 20151120, 5, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aula`
--

CREATE TABLE IF NOT EXISTS `aula` (
  `aul_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aul_nom` varchar(100) NOT NULL,
  `aul_insedu` varchar(200) NOT NULL COMMENT 'Institucion educativa',
  `aul_codmod` varchar(50) NOT NULL COMMENT 'Codigo modular',
  `aul_gra` bigint(20) NOT NULL,
  `aul_sec` varchar(50) NOT NULL,
  `aul_canusu` varchar(10) NOT NULL COMMENT 'Cantidad de usuarios',
  `aul_fec` bigint(20) NOT NULL,
  `usu_id` bigint(20) NOT NULL,
  `aul_est` varchar(5) NOT NULL,
  PRIMARY KEY (`aul_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `aula`
--

INSERT INTO `aula` (`aul_id`, `aul_nom`, `aul_insedu`, `aul_codmod`, `aul_gra`, `aul_sec`, `aul_canusu`, `aul_fec`, `usu_id`, `aul_est`) VALUES
(1, 'Leones', 'Fe y Alegria 04', '0102030405', 5, 'A', '15', 20141221, 1, '1'),
(2, 'AULA 2', 'Cesar Vallejo', '1215141618', 1, 'A', '12', 20141221, 1, '1'),
(3, 'Patitos', 'Nuestra Señora de la Merced', '0000000071', 6, 'A', '25', 20141223, 1, '0'),
(4, 'Elefantes', 'Daniel Alomias Robles', '8564201015', 3, 'A', '35', 20141223, 1, '1'),
(5, 'Aula Prueba', 'San Benito 5421', '5698201000', 1, 'A', '25', 20141223, 1, '1'),
(6, 'Ratitas', 'San Pablo II', '8541024500', 5, 'A', '30', 20141223, 1, '0'),
(7, 'Zebras', 'Daniel Alcides Carrion', '5498203652', 5, 'A', '11', 20141223, 1, '0'),
(8, 'Aula Gino', 'Cesar Vallejo del Per', '64987654', 5, 'C', '5', 20150418, 1, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivo`
--

CREATE TABLE IF NOT EXISTS `dispositivo` (
  `dis_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `usu_id` bigint(20) NOT NULL,
  `dis_cod` text NOT NULL,
  `dis_so` varchar(200) NOT NULL,
  `dis_fec` bigint(20) NOT NULL,
  `dis_est` varchar(5) NOT NULL,
  PRIMARY KEY (`dis_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Volcado de datos para la tabla `dispositivo`
--

INSERT INTO `dispositivo` (`dis_id`, `usu_id`, `dis_cod`, `dis_so`, `dis_fec`, `dis_est`) VALUES
(16, 4, 'APA91bEJJJlfykxj2uQ3ZPuo-ghRCfS870TJHohEU5MPt0_mMUnNxrpIvauqjxIGnLJ3Y3nPojwOl5DZJTrmjWOY9sdwK3MUtszcYmLbU0lpJnv7xlPg0bsyZHkKRsjfuYN0V_5ph60IQAFoqNFYQqCceQHJk_7zGw', 'android', 20150413, '1'),
(15, 22, '7ef025dc6bef2cd8be9896473885795589a9870d22f1eb2bdf2cef22b8790226', '', 20150413, '1'),
(3, 5, '2ac9b6b9fa80cc9aa7b809513af42bb454808761d29b87dea3411687b873adfe', '', 20150406, '1'),
(4, 10, 'APA91bFFr_U727csj3BBhaIokmgIT0SLEnaBbjfVsEnCGyEfaWoiFB5y0_BS42iKALs2a1XH_LIXpNpKUXWRN8Chf2ux9tOHDliJXFsTRl8c56E5AfGcMayZLEpuZDaS_IFxtPeNcW9vLYOrfvhRfF6-SeVuaaKgZw', 'android', 20150406, '1'),
(5, 6, 'fc6b1d46253b3d29c5e32af38a1d34c3cbae13f50eb3e96103fc73eda8f79a33', '', 20150406, '1'),
(6, 13, 'APA91bE92PEXAxg-Yxw0YvhNWgaOsYYVsO9o042czho7gXzX82E7ZYKxm9YaQzVN701XtqcLJxykuGXDzYtEqieTYZ_amaqlZF86_mB2FcU8CLrjHLHX72qGhc3GTAoU_kyOaR-qzmrJS5gzJuxG9F1l630C4dhTtA', 'android', 20150406, '1'),
(7, 8, 'APA91bF6CIfN4hcUNytfF_Qu03o04VxrOLD3wbmqImaMebCXjLNxGkB8CT7M01DW-U7jwFj0e566bFvoi94pdVHQKt1M7o2ghSJK4Wi9oFgwromja64jflqiatFKWe-vu0md7EUBcA8RSKRB_vT86IxTWvDFJpYfeQ', 'android', 20150406, '1'),
(8, 16, 'APA91bHw9nSmVJMpq5dOVKA64aaAZfHrUsNn8LWsgx-S2O9PJ4CbuE_rs33kFoYyBlZRE8cFFJqay-FS2ehoJL8vWPgbaMc7Ir8xYfcR13lFg5R2uy208AZSszp_wj8jSvmukbrnuAzXEjA1RAqp41oRSsGOEE4qMw', 'android', 20150406, '1'),
(9, 7, 'APA91bHmNpD7sxaJ2uucTaqGNOgjGhc78cymGRyDYCG2qZKRsd5rGqJIGbd--m7IGjKy8qFM3UBW70xp-aOK3RO5utpnydS9zxa95o5KaPviW2B8eFaySTKXcFMu41OY4Hjuab2PKvLbCtQjq52LPtrMEObmCBfHEw', 'android', 20150406, '1'),
(10, 19, 'APA91bHioQ_fxKeEwYYpyb6Y7pS4ofk_336AuiZemCB3-OMcw8IJQpvn391mOWjdHuG5E2_FKXjAiOBr-uXBn-rlSq2pJfGXS9U4-ALPdqTiSMgzmkLAKWr4noVl64FDQT9ZgOy27ktf_CBa8HN83-GglBnilxbtYA', 'android', 20150406, '1'),
(14, 23, 'APA91bE5JkFxCfX1LtB5EPmeKjmGqY0lGwtoY_louwBJd6__LcSg9c8e_pNijjahSC6sNV8qzi2EPqfx0JmhcLOow9zmgx3F37f7A90qIiUSyaNhz7JS1BzLwLRQG8Af7HCUvd7PZPOro8rwy-KVgE7nScPYvebw-w', 'android', 20150413, '1'),
(17, 3, '10991c51e3f6ab8d593652fb9f5bf455c956a94459bf4871698de9fa62e0c22d', '', 20150413, '1'),
(18, 3, '123456789123456', 'iOS', 20150414, '1'),
(19, 3, '1234567890', 'iOS', 20150414, '1'),
(20, 4, '1234567890', 'iOS', 20150414, '1'),
(21, 2, 'f0d2533136f2bf1812d1356913eae48792e94c024ee75d8250733857c430797d', 'iOS', 20150421, '1'),
(22, 3, 'APA91bE5JkFxCfX1LtB5EPmeKjmGqY0lGwtoY_louwBJd6__LcSg9c8e_pNijjahSC6sNV8qzi2EPqfx0JmhcLOow9zmgx3F37f7A90qIiUSyaNhz7JS1BzLwLRQG8Af7HCUvd7PZPOro8rwy-KVgE7nScPYvebw-w', 'android', 20150422, '1'),
(23, 3, '4b3b647e6673ebc801c00902d412caf78e29a07d540ed35757cab10bd488f6e0', 'iOS', 20150422, '1'),
(24, 3, '123123', 'android', 20150422, '1'),
(25, 4, '4b3b647e6673ebc801c00902d412caf78e29a07d540ed35757cab10bd488f6e0', 'iOS', 20150422, '1'),
(26, 2, 'dff4a9cffa136f84e4585bac317f2e13f35f8a59de1c9aeedfb9cd58cc4c32c4', 'iOS', 20150505, '1'),
(27, 3, '04b98cbf42a96d7b9869254d6688578d669a9d01c22df03ee3df3c14f689e523', 'iOS', 20150505, '1'),
(28, 4, '04b98cbf42a96d7b9869254d6688578d669a9d01c22df03ee3df3c14f689e523', 'iOS', 20150505, '1'),
(29, 3, 'APA91bFFqkazEdk1Cfi5EW8wo8sX1wpiKxP3PY0wcMmQPUbHFoMbAIcTTFoPwbJeSQxHkZuzu_KNpNPDdL8L4kuenV62tyVgn-Rxqag4Xfql4VkxhqLM3mDPnwPVmQ2NdYlKPZb00PDAVuuSGUz98w1ICmA2r9pLbQ', 'android', 20150510, '1'),
(30, 5, '2ac9b6b9fa80cc9aa7b809513af42bb454808761d29b87dea3411687b873adfe', 'iOS', 20150514, '1'),
(31, 24, 'APA91bFE8w_Dj-S3MLKTkMtC_UFX_uJphwWxBYuas50BJmoeYwd1U9xR9OmowRDU3BKd5qY7RY5zUY_3wpTtipaYayzzg_L9eANL-KHP7SRhBaFOjreis5vP60gk0l0q9RoimtT0H4rqrVCf0zYUOSHvlmB0t6fa5Q', 'android', 20150609, '1'),
(32, 25, 'APA91bFtv_7cXW9cMvEZJO9m2_D0fPyFfphrLZam7WD0ez9kRwiVKgVPK5eUibDiVkNmKxfWxnLXO31kW_esnnNt884hq0jI55aWpH0A1yusFEVUuSZz7LjH3axmFDI7G02lbkvZR0ezyKnB9flXB8EkvyWIdy_Xyw', 'android', 20150609, '1'),
(33, 27, 'APA91bHlx0cof6p7UgaLxtkCIIOBDU9XTvOmT20AmFnpIyXRbcp8kcQM8N__ypUo2szgerKHst_5qGr9Q-qtbfklG3fq7yknsrr62Iax2GPhj3XXFfQEgg8cI0W6T6GZE4A41w5O280meyZEAp1LK4j82C979my4ig', 'android', 20150609, '1'),
(34, 26, 'APA91bEFsEIFgBNILROFdoptm7OhxPxQIBzW1nd8nr2xQUQbemA5thgqOPSVWE06mZKkRiMoRfy4sPnK28KPjVrZ1qiPtMNx729xbp0zvdY_67WkU7FEwKn8WgXOZoHCm3Msg59mTb-jXeSwI4VArRXscRsFgQGA8g', 'android', 20150609, '1'),
(35, 28, 'APA91bEFsEIFgBNILROFdoptm7OhxPxQIBzW1nd8nr2xQUQbemA5thgqOPSVWE06mZKkRiMoRfy4sPnK28KPjVrZ1qiPtMNx729xbp0zvdY_67WkU7FEwKn8WgXOZoHCm3Msg59mTb-jXeSwI4VArRXscRsFgQGA8g', 'android', 20150609, '1'),
(36, 3, '5d572b3419a38eaa1265c22aa80ce3a04dbbe123980ad0c26ed43dc92447fa6b', 'iOS', 20150619, '1'),
(39, 3, '123123', '', 20150707, '1'),
(40, 3, '10991c51e3f6ab8d593652fb9f5bf455c956a94459bf4871698de9fa62e0c22d', 'iOS', 20150708, '1'),
(41, 3, '696ab818c7f2d7d298724b538d35cf53dd6ab1a93eddcb4c60e1694f09ccb955', 'iOS', 20150823, '1'),
(42, 3, '8659dd47b80396f73160469e1ce41b801f65f21dd01d21a3ba3ace73ecac372a', 'iOS', 20151024, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `usu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `usu_tip` bigint(20) NOT NULL COMMENT 'Tipo o Perfil',
  `usu_aul` bigint(20) NOT NULL COMMENT 'Aula',
  `usu_nom` varchar(50) NOT NULL,
  `usu_ape` varchar(100) NOT NULL,
  `usu_dni` varchar(10) NOT NULL,
  `usu_ema` varchar(50) NOT NULL,
  `usu_cel` varchar(50) NOT NULL,
  `usu_sex` varchar(5) NOT NULL,
  `usu_dir` varchar(200) NOT NULL,
  `usu_cla` varchar(200) NOT NULL,
  `usu_fec` bigint(20) NOT NULL,
  `usu_cod` bigint(20) NOT NULL,
  `usu_est` varchar(5) NOT NULL,
  `cookie` text NOT NULL,
  PRIMARY KEY (`usu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usu_id`, `usu_tip`, `usu_aul`, `usu_nom`, `usu_ape`, `usu_dni`, `usu_ema`, `usu_cel`, `usu_sex`, `usu_dir`, `usu_cla`, `usu_fec`, `usu_cod`, `usu_est`, `cookie`) VALUES
(1, 1, 0, 'Admin', 'Admin', '11111111', 'giacomo1203@gmail.com', '940625609', 'M', 'Av Brasil 469', 'kpOf', 20150406, 1, '1', ''),
(2, 2, 4, 'Roberto', 'Flores', '44444444', 'rflores@gmail.com', '987645987', 'M', 'Av Brasil 1203', 'lZai', 20150406, 1, '1', ''),
(3, 2, 5, 'Jose', 'Cruz', '12121212', 'giacomo1203@gmail.com', '940625609', 'M', 'Av bolichera 1205', 'kpOf', 20150406, 1, '1', '3385077'),
(4, 3, 5, 'Julian', 'Casablancas', '66666666', 'jcasablancas@gmail.com', '954321645', 'M', 'Los Angeles CA', 'kpOf', 20150406, 2, '1', ''),
(5, 2, 2, 'Adalberto', 'Acevedo', '07912188', 'adalbertoacevedo@gmail.com', '982598021', 'M', 'solari 222', 'xMLe1NXV09A=', 20150406, 1, '1', ''),
(6, 2, 4, 'Vanessa', 'Toribio', '10729113', 'vanetorib@gmail.com', '943508055', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(7, 3, 4, 'Gladys', 'Aguilar', '09356340', 'mensajeros-admi@hotmail.com', '980080916', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(8, 3, 4, 'Patricia', 'Navarro', '10316517', 'amanecer696a@hotmaail.com', '996891102', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(9, 3, 4, 'Jara', 'Repiso', '04221432', 'jara_repiso1@hotmail.com', '982598021', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(10, 3, 4, 'Carmen', 'Guzman', '06661459', 'gucetaavacar@hotmail.com', '968708312', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(11, 3, 4, 'Flor', 'Panta', '09824253', 'flory_1123@hotmail.com', '991279194', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(12, 3, 4, 'Elisa', 'Pariona', '07017379', 'elisacuba65@hotmail.com', '975318654', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(13, 3, 4, 'Nancy', 'Gomez', '06986609', 'nancy_65_30@hotmail.com', '991907527', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(14, 3, 4, 'Marilu', 'Rojas', '07026431', 'my_mar@hotmail.com', '986265788', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(15, 3, 4, 'Gabriela', 'Marpuez', '06649214', 'gaby_mv24@hotmail.com', '986947940', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(16, 3, 4, 'María', 'Lazaro', '08749065', 'marialazaroa15@gmail.com', '961803230', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(17, 3, 4, 'Rosa', 'Lin', '80133310', 'rosalinmelendezs11@gmail.com', '940042652', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(18, 3, 4, 'María', 'Fermin', '09489181', 'geminis1260@hotmail.com', '993907367', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(19, 3, 4, 'Jessica', 'Ayala', '08890285', 'jessicaayaala705@gmail.com', '962759419', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(20, 3, 4, 'Graciela', 'Anchiraico', '20707004', 'gracielaanchiraicop12@gmail.com', '965608755', 'F', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(21, 3, 4, 'Lizardo', 'Huaman', '10815228', 'lizaro1908@hotmail.com', '991634902', 'M', 'solari 222', 'kpOf', 20150406, 1, '1', ''),
(22, 3, 5, 'Jose', 'Sandoval', '13131313', 'jsandoval@gmail.com', '987987654', 'M', 'Av santa', 'kpOf', 20150413, 3, '1', ''),
(23, 3, 5, 'Robet', 'Dniro', '14141414', 'asd@gmail.com', '645978312', 'M', 'av lima peru', 'kpOf', 20150413, 3, '1', ''),
(24, 3, 2, 'Demo 1', 'Test', '01111111', 'demo@gmail.com', '978645654', 'M', 'Av demo 1', 'kpOf', 20150413, 1, '1', ''),
(25, 3, 2, 'Demo 2', 'Test', '02222222', 'demo@gmail.com', '978645654', 'M', 'Av demo 1', 'kpOf', 20150413, 1, '1', ''),
(26, 3, 2, 'Demo 3', 'Test', '03333333', 'demo@gmail.com', '978645654', 'M', 'Av demo 1', 'kpOf', 20150413, 1, '1', ''),
(27, 3, 2, 'Demo 4', 'Test', '04444444', 'demo@gmail.com', '978645654', 'M', 'Av demo 1', 'kpOf', 20150413, 1, '1', ''),
(28, 3, 2, 'Demo 5', 'Test', '05555555', 'demo@gmail.com', '978645654', 'M', 'Av demo 1', 'kpOf', 20150413, 1, '1', ''),
(29, 3, 2, 'Demo 6', 'Test', '06666666', 'demo@gmail.com', '978645654', 'M', 'Av demo 1', 'kpOf', 20150413, 1, '1', ''),
(30, 3, 2, 'Demo 7', 'Test', '07777777', 'demo@gmail.com', '978645654', 'M', 'Av demo 1', 'kpOf', 20150413, 1, '1', ''),
(31, 3, 2, 'Demo 8', 'Test', '08888888', 'demo@gmail.com', '978645654', 'M', 'Av demo 1', 'kpOf', 20150413, 1, '1', ''),
(32, 2, 8, 'Gino', 'Flores', '12341234', 'ginofl@hotmail.es', '954621320', 'M', 'Av las palmas del mundo', 'kpOf', 20150418, 1, '1', ''),
(33, 2, 1, 'Tutor', 'Demo', '14142525', 'admin-demo@gmail.com', '978654654', 'M', 'av lima 123', 'kpOfmQ==', 20150702, 1, '1', ''),
(34, 3, 1, 'Padre', 'Demo', '14142626', 'padredemo_@gmail.com', '987741852', 'M', 'Av Torre Negra 544', 'kpOfmQ==', 20150702, 1, '1', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
