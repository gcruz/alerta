<?php
	include("../sesion.php");
	include("../bd/bd.php");
	include("../bd/usuario.php");
	include("../inc/funcion.php");
	$obj_bd = new BD();
	$obj_usuario = new usuario();

	$modo = $_REQUEST['modo'];

	if($modo=="add" || $modo=="upd" || $modo=="pupd"){
		$tip = $_REQUEST['tip'];
		$aul = $_REQUEST['aul'];
		$nom = $_REQUEST['nom'];
		$ape = $_REQUEST['ape'];
		$dni = $_REQUEST['dni'];
		$ema = $_REQUEST['ema'];
		$cel = $_REQUEST['cel'];
		$sex = $_REQUEST['sex'];
		$dir = $_REQUEST['dir'];
		$cla = encriptar($_REQUEST['cla'], "alerta");
		$fec = date("Ymd");
		$usu = $c_id;
		$est = "1";

		if($modo=="add"){
			$res_usu = $obj_usuario->consultar("usu_id", "usu_dni='$dni'", "usu_id");
			if($obj_bd->num_rows($res_usu)>0){
				echo "E";
				die();
			}

			$id = $obj_usuario->grabar($tip, $aul, $nom, $ape, $dni, $ema, $cel, $sex, $dir, $cla, $fec, $usu, $est);
			header("location:../home.php?php=usuario");
		}elseif($modo=="upd"){
			$id = $_REQUEST['id'];
			$obj_usuario->actualizar($id, $tip, $aul, $nom, $ape, $dni, $ema, $cel, $sex, $dir, $cla);
			header("location:../home.php?php=usuario");
		}elseif($modo=="pupd"){
			$id = $_REQUEST['id'];
			$obj_usuario->actualizar($id, $tip, $aul, $nom, $ape, $dni, $ema, $cel, $sex, $dir, $cla);
			echo "Perfil actualizado correctamente.";
		}
	}elseif($modo=="del"){
		$id  = $_REQUEST['id'];
		$est = $_REQUEST['est'];
		$obj_bd->proceso("update usuario set usu_est='$est' where usu_id=$id");
	}
?>