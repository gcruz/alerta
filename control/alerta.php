<?php
	include("../sesion.php");
	include("../bd/bd.php");
	include("../bd/alerta.php");
	include("../bd/usuario.php");
	include("../inc/funcion.php");
	$obj_bd = new BD();
	$obj_alerta = new alerta();
	$obj_usuario = new usuario();

	$rootPem = "../servicios/apn/Alerta.pem";

	$modo = $_REQUEST['modo'];

	if($modo=="add" || $modo=="upd"){
		$asu = $_REQUEST['asu'];
		$des = ",".$_REQUEST['des'].",";
		$pri = $_REQUEST['pri'];
		$men = $_REQUEST['men'];
		$hor = date("H:i:s");
		$fec = date("Ymd");
		$usu = $c_id;
		$est = "1";
	
		if($modo=="add"){
			$id = $obj_alerta->grabar($asu, $des, $pri, $men, $hor, $fec, $usu, $est);
            include("../servicios/gcm.php");
            include("../servicios/apn/sendNotificacion.php");
			echo "G";
		}elseif($modo=="upd"){
			$id = $_REQUEST['id'];
			$obj_alerta->actualizar($id, $asu, $des, $pri, $men, $hor);
			echo "A";
		}
	}elseif($modo=="del"){
		$id = $_REQUEST['id'];
		$obj_bd->proceso("update alerta set ale_est='0' where ale_id=$id");
	}
?>